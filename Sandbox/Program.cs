﻿using System;
using System.Linq;
using System.IO;
using System.Xml.Serialization;
using FixedWidth.FixedPO;

namespace Sandbox
{
    class Program
    {
        static void Main(string[] args)
        {
            Header h = new Header();
            h[0].Content = "Order";
            h[1].Content = "1293847";
            h[2].Content = "and some more";

            FixedPO po = new FixedPO();
            po.Header = h;

            for (int i = 0; i < 10; i++) {
                LineItem it = new LineItem();
                it["id"] = i.ToString();
                it["price"] = (i * 2).ToString("#.00");
                it["description"] = $"An item #{i}";
                it["vn"] = $"2843{i}983";
                it["in"] = $"4938{i}";
                po.LineItems.Add(it);
            }

            Console.WriteLine($"h[\"PONumber\"] == {h["PONumber"]}");
            Console.WriteLine($"h[\"RecordType\"] == {h["RecordType"]}");
            Console.WriteLine(po);

            XmlSerializer x = new XmlSerializer(typeof(FixedPO));
            using (TextWriter w = new StreamWriter("ha.xml")) {
                x.Serialize(w, po);
            }

            using (var w = new StreamWriter("po.dat")) {
                w.WriteLine(po);
            }

        }
    }
}
