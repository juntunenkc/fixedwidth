﻿using System;
using System.Xml.Serialization;

namespace FixedWidth
{
    [Serializable()]
    public class Field
    {
        int position = -1;
        int length = -1;
        string content = string.Empty;

        [XmlAttribute()]
        public string Name { get; set;}

        public string Content {
            get {
                return content;
            }
            set {
                content = value;
            }
        }

        public int Position {
            get {
                return position;
            }
            set {
                position = value;
            }
        }

        public int Length {
            get {
                return length;
            }
            set {
                length = value;
            }
        }

        public Field(int pos, int len) : this(pos, len, string.Empty) { }

        public Field() : this(-1, -1, string.Empty) { }

        public Field(int pos, int len, string cont) {
            position = pos;
            length = len;
            content = cont;
        }

        public override string ToString() {
            string output = content.PadLeft(length);
            if (content.Length > length) output = content.Substring(0, length);
            return output;
        }
    }
}
