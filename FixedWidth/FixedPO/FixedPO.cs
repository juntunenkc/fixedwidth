using System;
using System.Text;
using System.Collections.Generic;

namespace FixedWidth.FixedPO {
    [Serializable()]
    public class FixedPO {
        public Header Header { get; set; }
        public List<LineItem> LineItems { get; set; }

        public FixedPO() {
            LineItems = new List<LineItem>();
        }

        public override string ToString() {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(Header.ToString());
            foreach (var item in LineItems) {
                sb.AppendLine(item.ToString());
            }
            return sb.ToString();
        }
    }
}
