using System;

namespace FixedWidth.FixedPO {
    [Serializable()]
    public class LineItem : Record {
        public LineItem() {
            Add(new Field(0, 6, "item") { Name = "RecordType" });
            Add(new Field(7, 14) { Name = "id" });
            Add(new Field(15, 20) { Name = "price" });
            Add(new Field(21, 50) { Name = "description" });
            Add(new Field(51, 60) { Name = "vn" });
            Add(new Field(61, 70) { Name = "in" });
        }
    }
}
