using System;

namespace FixedWidth.FixedPO
{
    [Serializable()]
    public class Header : Record
    {
        public Header()
        {
            Add(new Field(0, 6, "Order") { Name = "RecordType" });
            Add(new Field(7, 12) { Name = "PONumber" } );
            Add(new Field(19, 22) { Name = "Description" } );
        }
    }
}
