using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;

namespace FixedWidth
{
    [Serializable()]
    public class Record : IList<Field> {
        List<Field> _fields = new List<Field>();

        public override string ToString() {
            StringBuilder sb = new StringBuilder();
            foreach (var field in _fields) {
                while (field.Position > sb.Length) sb.Append(" ");
                sb.Append(field);
            }
            return sb.ToString();
        }

        public Field this[int index] { get => ((IList<Field>)_fields)[index]; set => ((IList<Field>)_fields)[index] = value; }

        public string this[string name] {
            get  {
                var ff = _fields.Where(f => f.Name == name);
                if (ff.Count() != 1)
                    throw new FormatException($"{ff.Count()} fields with name `{name}'!");
                return ff.First().Content;
            }
            set {
                var ff = _fields.Where(f => f.Name == name);
                if (ff.Count() != 1)
                    throw new FormatException($"{ff.Count()} fields with name `{name}'!");
                var field = ff.First();
                field.Content = value;
            }
        }

        public int Count => ((IList<Field>)_fields).Count;

        public bool IsReadOnly => ((IList<Field>)_fields).IsReadOnly;

        public void Add(Field item)
        {
            ((IList<Field>)_fields).Add(item);
        }

        public void Clear()
        {
            ((IList<Field>)_fields).Clear();
        }

        public bool Contains(Field item)
        {
            return ((IList<Field>)_fields).Contains(item);
        }

        public void CopyTo(Field[] array, int arrayIndex)
        {
            ((IList<Field>)_fields).CopyTo(array, arrayIndex);
        }

        public IEnumerator<Field> GetEnumerator()
        {
            return ((IList<Field>)_fields).GetEnumerator();
        }

        public int IndexOf(Field item)
        {
            return ((IList<Field>)_fields).IndexOf(item);
        }

        public void Insert(int index, Field item)
        {
            ((IList<Field>)_fields).Insert(index, item);
        }

        public bool Remove(Field item)
        {
            return ((IList<Field>)_fields).Remove(item);
        }

        public void RemoveAt(int index)
        {
            ((IList<Field>)_fields).RemoveAt(index);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IList<Field>)_fields).GetEnumerator();
        }
    }
}
